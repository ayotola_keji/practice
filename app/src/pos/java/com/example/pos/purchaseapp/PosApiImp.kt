package com.example.pos.purchaseapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.example.purchaseapp.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject


/**
 * Traditional POS implementation of the [PosApi]
 */
class PosApiImp @Inject constructor(private val json: Json) : PosApi {
    override fun transactionContract(): PosPaymentContract = PosCardTransactionContract(json)

}

private class PosCardTransactionContract(private val json: Json) : PosPaymentContract() {
    override fun createIntent(context: Context, input: TransactionRequest?): Intent {
        requireNotNull(input) { "request must not be null" }
        return Intent(Constants.TRANSACTION_ACTION)
            .putExtra(Constants.REQUEST_DATA_KEY, json.encodeToString(input))
    }

    override fun parseResult(resultCode: Int, intent: Intent?): PaymentResult {
        if (resultCode != Activity.RESULT_OK || intent == null) return PaymentResult(
            Status.CANCELLED, null, "Cancelled"
        )

        val data = intent.getStringExtra(Constants.RESPONSE_DATA_KEY)
        val status = intent.getStringExtra(Constants.RESPONSE_STATUS_KEY)
        val message = intent.getStringExtra(Constants.RESPONSE_STATUS_MESSAGE_KEY)

        return parseTransactionResult(status, data, message)

    }

    private fun parseTransactionResult(status: String?, data: String?, statusMessage: String?): PaymentResult {
        val transactionStatus = when (status) {
            Constants.STATUS_SUCCESS -> Status.APPROVED
            Constants.STATUS_FAILED -> Status.DECLINED
            Constants.STATUS_CANCELLED -> Status.CANCELLED
            Constants.STATUS_TIMEOUT -> Status.TIMEOUT
            else -> Status.FAILED
        }
        val response = if (data != null) {
            runCatching { json.decodeFromString<TransactionResponse>(data) }.getOrNull()
        } else null

        return PaymentResult(transactionStatus, response, statusMessage)

    }
}

private const val KEY_EXTRA_REQUEST_DATA = "requestData"
private const val INTENT_TRANSACTION = "com.globalaccelerex.transaction"