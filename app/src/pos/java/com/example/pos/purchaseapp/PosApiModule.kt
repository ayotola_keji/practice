package com.example.pos.purchaseapp

import com.example.purchaseapp.PosApi
import dagger.Binds
import dagger.Module

@Module
interface PosApiModule {

    @Binds
    fun bindsPosOnlyApi(posOnlyApiImp: PosApiImp): PosApi

}