package com.example.mobile.purchaseapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.example.purchaseapp.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject
import javax.inject.Singleton

/**
 * The mobile pos implementation for the [PosApi]
 */
@Singleton
class MobilePosApiImp @Inject constructor() : PosApi {

    override fun transactionContract(): PosPaymentContract = MobilePosTransactionContract()
}

/**
 * The mobile pos transaction contract translates the requests and responses between the app and the mobile pos payment APIs.
 * This is necessary as the mpos payment apis already uses activity results contract.
 */
private class MobilePosTransactionContract : PosPaymentContract() {

    override fun createIntent(context: Context, input: TransactionRequest?): Intent {
        requireNotNull(input) { "Request must not be null" }
        return Intent(Constants.TRANSACTION_ACTION)
            .putExtra(Constants.REQUEST_DATA_KEY, Json.encodeToString(input))
    }

    override fun parseResult(resultCode: Int, intent: Intent?): PaymentResult {
        if (resultCode != Activity.RESULT_OK || intent == null) return PaymentResult(
            Status.CANCELLED, null, "Cancelled"
        )
        val data = intent.getStringExtra(Constants.RESPONSE_DATA_KEY)
        val status = intent.getStringExtra(Constants.RESPONSE_STATUS_KEY)
        val message = intent.getStringExtra(Constants.RESPONSE_STATUS_MESSAGE_KEY)

        return parseTransactionResult(status, data, message)

    }

    private fun parseTransactionResult(
        status: String?,
        data: String?,
        statusMessage: String?,
    ): PaymentResult {
        val transactionStatus = when (status) {
            Constants.STATUS_SUCCESS -> Status.APPROVED
            Constants.STATUS_FAILED -> Status.DECLINED
            Constants.STATUS_CANCELLED -> Status.CANCELLED
            Constants.STATUS_TIMEOUT -> Status.TIMEOUT
            else -> Status.FAILED
        }
        val response = if (data != null) {
            runCatching { Json.decodeFromString<TransactionResponse>(data) }.getOrNull()
        } else null

        return PaymentResult(transactionStatus, response, statusMessage)

    }

}
