package com.example.mobile.purchaseapp

import com.example.purchaseapp.PosApi
import dagger.Binds
import dagger.Module

@Module
interface PosApiModule {

    @Binds
    fun bindsPosApi(mobilePosApiImp: MobilePosApiImp): PosApi
}