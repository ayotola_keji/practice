package com.example.purchaseapp

enum class Status {
    /**
     * Transaction successfully approved
     */
    APPROVED,

    /**
     * Transaction was declined by the issuer bank.
     */
    DECLINED,

    /**
     * Transaction failed ultimately for some reason.
     */
    FAILED,

    /**
     * The transaction was cancelled by the user or as a result of not getting response from user on time
     */
    CANCELLED,

    /**
     * A timeout occurred while waiting for response from switch/issuer
     */
    TIMEOUT
}