package com.example.purchaseapp

import android.content.Context

object Injector {

    private lateinit var applicationContext: Context

    private lateinit var appComponent: AppComponent

    fun init(context: Context) {
        applicationContext = context.applicationContext
    }

    fun get(): AppComponent {
        if (!::appComponent.isInitialized) {
            synchronized(this) {
                if (!::appComponent.isInitialized) {
                    appComponent = DaggerAppComponent.factory().create(applicationContext)
                }
            }
        }
        return appComponent
    }
}