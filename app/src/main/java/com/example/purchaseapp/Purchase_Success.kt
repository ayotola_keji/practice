package com.example.purchaseapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.purchaseapp.databinding.ActivityPurchaseSuccessBinding

class Purchase_Success : AppCompatActivity() {

    private lateinit var binding: ActivityPurchaseSuccessBinding
    private lateinit var doneBtn: Button
    private lateinit var succexText: TextView
    private lateinit var succexLogo: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPurchaseSuccessBinding.inflate(layoutInflater)
        setContentView(binding.root)

        doneBtn = binding.button
        succexText = binding.headingTextView
        succexLogo = binding.imageView

        doneBtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}