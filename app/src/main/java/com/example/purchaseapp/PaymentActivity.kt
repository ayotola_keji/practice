package com.example.purchaseapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.purchaseapp.databinding.ActivityPaymentActivityBinding
import timber.log.Timber


class PaymentActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPaymentActivityBinding
    private lateinit var amounting: TextView
    private lateinit var amountInput: EditText
    private lateinit var purchaseBtn: Button

    private val posApi = Injector.get().posApi()

    private val purchaseCall = registerForActivityResult(posApi.transactionContract()) { result ->
        Timber.d(result.response.toString())
        if (result.status == Status.APPROVED || result.status == Status.DECLINED) {
            if (result?.response != null) {
                Toast.makeText(this@PaymentActivity, "Payment was successful", Toast.LENGTH_LONG)
                    .show()
                val intent = Intent(this, Purchase_Success::class.java)
                startActivity(intent)
            } else {
                val errorMessage = result.message ?: "Payment Failed"
                Toast.makeText(this@PaymentActivity, errorMessage, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        amounting = binding.amount
        amountInput = binding.amountInput
        purchaseBtn = binding.payButton

        purchaseBtn.setOnClickListener {
            val amount = amountInput.text.toString()
            val request = TransactionRequest("PURCHASE", amount, "false", null)
            try {
                purchaseCall.launch(request)
            } catch (e: Exception) {
                Timber.d(e, "Throw error")
            }

        }

    }


}