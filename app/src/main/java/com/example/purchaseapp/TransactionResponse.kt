package com.example.purchaseapp

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
@Parcelize
data class TransactionResponse(
    @SerialName("aid") val aid: String? = null,
    @SerialName("amount") val amount: String? = null,
    @SerialName("appLabel") val appLabel: String? = null,
    @SerialName("authcode") val authCode: String? = null,
    @SerialName("bankLogo") val bankLogo: String? = null,
    @SerialName("bankName") val bankName: String? = null,
    @SerialName("baseAppVersion") val baseAppVersion: String? = null,
    @SerialName("cardExpireDate") val cardExpireDate: String? = null,
    @SerialName("cardHolderName") val cardHolderName: String? = null,
    @SerialName("cashBackAmount") val cashBackAmount: String? = null,
    @SerialName("datetime") val datetime: String? = null,
    @SerialName("deviceSerialNumber") val deviceSerialNumber: String? = null,
    @SerialName("footerMessage") val footerMessage: String? = null,
    @SerialName("maskedPan") val maskedPan: String? = null,
    @SerialName("merchantAddress") val merchantAddress: String? = null,
    @SerialName("merchantCategoryCode") val merchantCategoryCode: String? = null,
    @SerialName("merchantId") val merchantId: String? = null,
    @SerialName("merchantName") val merchantName: String? = null,
    @SerialName("message") val message: String? = null,
    @SerialName("nuban") val nuban: String? = null,
    @SerialName("pinType") val pinType: String? = null,
    @SerialName("ptsp") val ptsp: String? = null,
    @SerialName("ptspContact") val ptspContact: String? = null,
    @SerialName("rrn") val rrn: String? = null,
    @SerialName("stan") val stan: String? = null,
    @SerialName("statuscode") val statusCode: String? = null,
    @SerialName("terminalID") val terminalID: String? = null,
    @SerialName("transactionType") val transactionType: String? = null,
    @SerialName("currency") val currency: String? = null
): Parcelable {
    fun approved(): Boolean = statusCode == "00"
    }

@Keep
@Serializable
data class TransactionRequest(
    @SerialName("transType") val transactionType: String = "",
    @SerialName("amount") val amount: String? = "",
    @SerialName("print") val print: String,
    @SerialName("transactionReference") val transactionReference: String? = "",
)

data class PaymentResult(
    val status: Status,
    val response: TransactionResponse?,
    val message: String?
)