package com.example.purchaseapp

import com.example.mobile.purchaseapp.MobilePosApiImp
import dagger.Binds
import dagger.Module

@Module
interface PosApiModule {

    @Binds
    fun bindsPosApi(mobilePosApiImp: MobilePosApiImp): PosApi
}