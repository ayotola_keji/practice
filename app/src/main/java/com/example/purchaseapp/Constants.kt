package com.example.purchaseapp

object Constants {

    const val TRANSACTION_ACTION = "com.globalaccelerex.transaction"
    const val REQUEST_DATA_KEY = "requestData"
    const val RESPONSE_DATA_KEY = "data"
    const val RESPONSE_STATUS_KEY = "status"
    const val RESPONSE_STATUS_MESSAGE_KEY = "statusMessage"
    const val STATUS_FAILED = "02"
    const val STATUS_SUCCESS = "00"
    const val STATUS_CANCELLED = "03"
    const val STATUS_INVALID = "04"
    const val STATUS_WRONG_PARAM = "05"
    const val STATUS_TIMEOUT = "06"

    private val statusMessages: Map<String, String> = mapOf(
        STATUS_SUCCESS to "Approved",
        STATUS_FAILED to "Declined",
        STATUS_CANCELLED to "Cancel",
        STATUS_INVALID to "Invalid Format",
        STATUS_WRONG_PARAM to "Wrong parameter",
        STATUS_TIMEOUT to "Timeout"
    )

    fun statusMessage(status: String) = statusMessages[status]
}

