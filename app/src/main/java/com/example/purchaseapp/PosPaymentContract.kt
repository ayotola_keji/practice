package com.example.purchaseapp

import androidx.activity.result.contract.ActivityResultContract

/**
 * A transaction contract that is used to initiate a card payment transaction.
 */
typealias PosPaymentContract = ActivityResultContract<TransactionRequest, PaymentResult>